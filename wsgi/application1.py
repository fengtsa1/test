'''
import cherrypy
class HelloWorld(object):
    def index(self):
        return "I come,I see,I conquer~"
    index.exposed = True

#cherrypy.quickstart(HelloWorld())
application = cherrypy.Application(HelloWorld())
'''
#coding: utf-8
import cherrypy
import os
# for mako
#from mako.template import Template
from mako.lookup import TemplateLookup

# site_config could be "OpenShift" or "local"
# 將 site_config 設為 "OpenShift" 表示程式將在 OpenSite 平台中執行, 否則就是在 local 執行
site_config = "OpenShift"
if site_config == "OpenShift":
    SQLite_data_dir = os.environ['OPENSHIFT_DATA_DIR']+"/db"
    download_root_dir = os.environ['OPENSHIFT_DATA_DIR']
    template_root_dir = os.environ['OPENSHIFT_REPO_DIR']+"/wsgi/static"
else:
    目前目錄 = os.getcwd()
    print(目前目錄)
    SQLite_data_dir = 目前目錄+"\\pyforum_db"
    download_root_dir = 目前目錄
    template_root_dir = 目前目錄

class HelloWorld(object):
    _cp_config = {
    # 配合 utf-8 格式之表單內容
    # 若沒有 utf-8 encoding 設定,則表單不可輸入中文
    'tools.encode.encoding': 'utf-8',
    # 加入 session 設定
    'tools.sessions.on' : True,
    'tools.sessions.storage_type' : 'file',
    'tools.sessions.locking' : 'explicit',
    # 就 OpenShift ./tmp 位於 app-root/runtime/repo/tmp
    'tools.sessions.storage_path' : './tmp',
    # 內定的 session timeout 時間為 60 分鐘
    'tools.sessions.timeout' : 60,
    # 以下透過  @cherrypy.tools.mako(filename="mytmpl.txt") decorator 設定的套稿目錄依據
    # javascript 檔案位於 templates/jscript 目錄下, css 檔案則位於 templates/style 目錄下
    'tools.mako.directories' :  template_root_dir+"/templates"
    }

    @cherrypy.expose
    def index(self, stud_number=None, stud_name=None):
        #return "Hello World!"
        套稿查詢 = TemplateLookup(directories=[template_root_dir+"/templates"])
        # 必須要從 templates 目錄取出 index.html
        內建頁面 = 套稿查詢.get_template("index.html")
        return 內建頁面.render(stud_number=stud_number, stud_name=stud_name)

application_conf = {'/kmol':{
        'tools.staticdir.on': True,
        'tools.staticdir.dir': os.environ['OPENSHIFT_DATA_DIR']+"/kmol"},
        '/downloads':{
        'tools.staticdir.on': True,
        'tools.staticdir.root': download_root_dir,
        'tools.staticdir.dir': 'downloads',
        'tools.staticdir.index' : 'index.htm'
        },
        # 設定靜態 templates 檔案目錄對應
        '/templates':{
        'tools.staticdir.on': True,
        'tools.staticdir.root': template_root_dir,
        'tools.staticdir.dir': 'templates',
        'tools.staticdir.index' : 'index.htm'
        }
    }

#cherrypy.quickstart(HelloWorld())
application = cherrypy.Application(HelloWorld(), config = application_conf)
# 根據 site_config 的設定, 可以分別在 OpenShift 與 local 運行
if site_config == "OpenShift":
    application = cherrypy.Application(HelloWorld(), config = application_conf)
else:
    cherrypy.quickstart(HelloWorld(), config = application_conf)
